package datacollection.dic.datacollection;

import android.content.Context;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class RecordingActivity extends AppCompatActivity {
    Context mContext;
    Recording mRecord;
    //UI Elements
    Button mStartButton;
    Button mStopButton;
    Button mSubmitIntoDB;

    RadioGroup mRadioGroupTrafficFlow;
    RadioGroup mRadioGroupAirCondition;
    RadioGroup mRadioGroupClimate;
    RadioGroup mRadioGroupPhoneStatus;

    AutoCompleteTextView mRoughLocation;
    AutoCompleteTextView mDistanceFromLocation;
    AutoCompleteTextView mRemarks;

    Chronometer mChronometer;

    String trafficFlow;
    String airCondition;
    String climate;
    String phoneStatus;
    String roughLocation;
    String fineLocation;
    String distanceFromLocation;
    String remarks;

    DatabaseAdapter databaseHelper;

    Bundle extras;
    String mode;

    String fileName ,startTime,stopTime,startMillis,stopMillis;


    long start,stop;
    long duration,totalDataLen;

    private String[] dataFromStartRecording;
    private String[] dataFromStopRecording;

    private long lastPause;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording);
        mContext = this;
        //Create  Recorder Instance
        mRecord = new Recording(mContext);

        databaseHelper = new DatabaseAdapter(this);

        mChronometer = (Chronometer)findViewById(R.id.timer);


        // Button Handlers
        mStartButton = (Button)findViewById(R.id.StartButton);
        mStopButton = (Button)findViewById(R.id.StopButton);

        //Form Elements
        mRadioGroupTrafficFlow = (RadioGroup)findViewById(R.id.radioGroupTrafficFlow);
        mRadioGroupAirCondition = (RadioGroup)findViewById(R.id.radioGroupAirCondition);
        mRadioGroupClimate = (RadioGroup)findViewById(R.id.radioGroupClimate);
        mRadioGroupPhoneStatus = (RadioGroup)findViewById(R.id.radioGroupPhoneStatus);
        mRoughLocation = (AutoCompleteTextView) findViewById(R.id.roughLocation);
        mDistanceFromLocation = (AutoCompleteTextView) findViewById(R.id.distanceFromLocation);
        mRemarks = (AutoCompleteTextView) findViewById(R.id.remarks);
        mSubmitIntoDB = (Button)findViewById(R.id.submitIntoDB);

        //Extra
        final View targetView = findViewById(R.id.form);

        extras = getIntent().getExtras();
        mode = extras.getString("KEY");

        mStopButton.setEnabled(false);


        // Button Click Listeners
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataFromStartRecording = mRecord.startRecording();
                mStartButton.setEnabled(false);
                mStopButton.setEnabled(true);
                fileName = dataFromStartRecording[2];
                startTime = dataFromStartRecording[0];
                startMillis  =dataFromStartRecording[1];
                start = Long.parseLong(startMillis);
                timerReset();
                timerStart();


            }
        });

        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataFromStopRecording = mRecord.stopRecording();
                timerStop();
                mStartButton.setEnabled(true);
                mStopButton.setEnabled(false);
                targetView.getParent().requestChildFocus(targetView,targetView);
                stopTime = dataFromStopRecording[0];
                stopMillis = dataFromStopRecording[1];
                totalDataLen = Long.parseLong(dataFromStopRecording[2]);
                stop = Long.parseLong(stopMillis);
                duration = stop - start;



            }
        });



        mSubmitIntoDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                trafficFlow = ((RadioButton)findViewById(mRadioGroupTrafficFlow.getCheckedRadioButtonId())).getText().toString();
                airCondition = ((RadioButton)findViewById(mRadioGroupAirCondition.getCheckedRadioButtonId())).getText().toString();
                climate = ((RadioButton)findViewById(mRadioGroupClimate.getCheckedRadioButtonId())).getText().toString();
                phoneStatus = ((RadioButton)findViewById(mRadioGroupPhoneStatus.getCheckedRadioButtonId())).getText().toString();
                roughLocation = mRoughLocation.getText().toString();
                //fineLocation = mFineLocation.getText().toString();
                distanceFromLocation = mDistanceFromLocation.getText().toString();
                remarks = mRemarks.getText().toString();

                long id = databaseHelper.insertData(mode, fileName, startTime, stopTime, duration,totalDataLen,trafficFlow,airCondition,climate,phoneStatus,roughLocation,fineLocation,distanceFromLocation,remarks);
                if (id < 0) {
                    Message.message(getBaseContext(),"Unsuccessful");
                } else {
                    Message.message(getBaseContext(),"Successfully inserted one Row");
                }

            }
        });




    }

    private void timerStart(){
        if(lastPause!=0){
            mChronometer.setBase(mChronometer.getBase()+ SystemClock.elapsedRealtime()-lastPause);
        }else{
            mChronometer.setBase(SystemClock.elapsedRealtime());
        }
        mChronometer.start();

    }

    private void timerStop(){
        mChronometer.stop();
        lastPause=0;
    }

    private void timerReset(){
        mChronometer.setBase(0);
    }







}

